package fr.dl.quizzdenis.DAO;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;

import fr.dl.quizzdenis.Metier.Reponse;

public class DAOReponse {

    private static final int VERSION = 1;
    private static final String NOM_BDD = "quizzdenis.sqlite";
    private static final String TABLE = "reponse";
    private static final String COL_ID = "id";
    private static final String COL_LIBELLE = "libelle";
    private static final String COL_ISCORRECTE = "iscorrecte";
    private static final String COL_IDQUESTION = "id_question";
    private SQLiteDatabase bdd;
    private QuizzSQLite quizzSQLite;

    public DAOReponse(Context context) {
        quizzSQLite = QuizzSQLite.getInstance(context);
    }

    public void openForWrite() {
        bdd = quizzSQLite.getWritableDatabase();
    }

    private void openForRead() {
        bdd = quizzSQLite.getReadableDatabase();
    }

    public void close() {
        bdd.close();
    }

    public ArrayList<Reponse> getAllReponsesByIdQuestion(int idQuestion) {

        openForRead();
        ArrayList<Reponse> listeReponses = new ArrayList<>();
        Cursor c = bdd.query(TABLE, new String[]{COL_ID, COL_LIBELLE, COL_ISCORRECTE, COL_IDQUESTION}, COL_IDQUESTION + " =\"" + idQuestion + "\"", null, null,
                null, COL_ID);

        if (c.getCount() == 0) {
            c.close();
            return null;
        }

        while (c.moveToNext()) {
            Reponse reponse = cursorToReponse(c);
            listeReponses.add(reponse);
        }
        c.close();
        return listeReponses;
    }

    private Reponse cursorToReponse(Cursor c) {
        return new Reponse(c.getInt(0), c.getString(1), c.getInt(2));
    }

}
