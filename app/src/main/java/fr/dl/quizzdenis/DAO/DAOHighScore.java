package fr.dl.quizzdenis.DAO;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;

import fr.dl.quizzdenis.Metier.HighScore;

public class DAOHighScore {

    private static final int VERSION = 1;
    private static final String NOM_BDD = "quizzdenis.sqlite";
    private static final String TABLE = "highscore";
    private static final String COL_ID = "id";
    private static final String COL_NOMJOUEUR = "nom_joueur";
    private static final String COL_SCORE = "score";
    private static final String COL_IDTHEME = "id_theme";
    private static final String COL_DIFFICULTE = "difficulte";
    private SQLiteDatabase bdd;
    private QuizzSQLite quizzSQLite;

    public DAOHighScore(Context context) {
        quizzSQLite = QuizzSQLite.getInstance(context);
    }

    private void openForWrite() {
        bdd = quizzSQLite.getWritableDatabase();
    }

    private void openForRead() {
        bdd = quizzSQLite.getReadableDatabase();
    }

    public void close() {
        bdd.close();
    }

    public ArrayList<HighScore> getAllHighScores(int idTheme, int difficulte) {

        openForRead();
        ArrayList<HighScore> listeScores = new ArrayList<>();
        Cursor c = bdd.query(TABLE, new String[]{COL_ID, COL_NOMJOUEUR, COL_SCORE, COL_IDTHEME, COL_DIFFICULTE}, COL_IDTHEME + " = " + idTheme + " AND " + COL_DIFFICULTE + " = " + difficulte,
                null, null, null, COL_SCORE + " DESC LIMIT 3");

        if (c.getCount() == 0) {
            c.close();
            return null;
        }

        while (c.moveToNext()) {
            HighScore score = cursorToScore(c);
            listeScores.add(score);
        }

        c.close();
        return listeScores;
    }

    public boolean insertNewScore(HighScore score) {

        boolean isOK;
        ContentValues insertValues = new ContentValues();
        int id = getMaxId();
        insertValues.put("id", id);
        insertValues.put("nom_joueur", score.getNomJoueur());
        insertValues.put("score", score.getScore());
        insertValues.put("id_theme", score.getIdTheme());
        insertValues.put("difficulte", score.getDifficulte());

        openForWrite();
        try {
            bdd.insert(TABLE, null, insertValues);
            isOK = true;
        } catch (Exception e) {
            e.printStackTrace();
            isOK = false;
        }
        close();
        return isOK;
    }

    private int getMaxId() {

        String query = "select (max(id) + 1) from highscore";
        openForRead();
        Cursor c = bdd.rawQuery(query, null);
        c.moveToFirst();
        float maxId = c.getFloat(0);
        c.close();
        return (int) maxId;
    }

    private HighScore cursorToScore(Cursor c) {
        return new HighScore(c.getInt(0), c.getString(1), c.getInt(2), c.getInt(3), c.getInt(4));
    }

}

