package fr.dl.quizzdenis.DAO;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;

import fr.dl.quizzdenis.Metier.Theme;

public class DAOTheme {

    private static final int VERSION = 1;
    private static final String NOM_BDD = "quizzdenis.sqlite";
    private static final String TABLE = "theme";
    private static final String COL_ID = "id";
    private static final String COL_LIBELLE = "libelle";
    private static final String COL_ICONE = "icone";
    private SQLiteDatabase bdd;
    private QuizzSQLite quizzSQLite;

    public DAOTheme(Context context) {
        quizzSQLite = QuizzSQLite.getInstance(context);
    }

    public void openForWrite() {
        bdd = quizzSQLite.getWritableDatabase();
    }

    public void openForRead() {
        bdd = quizzSQLite.getReadableDatabase();
    }

    public void close() {
        bdd.close();
    }

    public Theme getThemeByID(int id) {
        openForRead();
        Cursor c = bdd.query(TABLE, new String[]{COL_ID, COL_LIBELLE, COL_ICONE}, COL_ID + " = \"" + id + "\"", null, null,
                null, COL_LIBELLE);
        Theme theme = cursorToTheme(c);
        c.close();
        return theme;
    }

    public ArrayList<Theme> getAllThemes() {
        openForRead();
        Cursor c = bdd.query(TABLE, new String[]{COL_ID, COL_LIBELLE, COL_ICONE}, null, null, null, null, COL_LIBELLE);

        if (c.getCount() == 0) {
            c.close();
            return null;
        }

        ArrayList<Theme> themesList = new ArrayList<>();
        while (c.moveToNext()) {
            Theme theme = cursorToTheme(c);
            themesList.add(theme);
        }

        c.close();
        return themesList;
    }

    private Theme cursorToTheme(Cursor c) {
        return new Theme(c.getInt(0), c.getString(1), c.getString(2));
    }

}
