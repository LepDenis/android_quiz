package fr.dl.quizzdenis.DAO;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;

import fr.dl.quizzdenis.Metier.Question;

public class DAOQuestion {

    private static final int VERSION = 1;
    private static final String NOM_BDD = "quizzdenis.sqlite";
    private static final String TABLE = "question";
    private static final String COL_ID = "id";
    private static final String COL_LIBELLE = "libelle";
    private static final String COL_IDTHEME = "id_theme";
    private static final String COL_DIFFICULTE = "difficulte";
    private SQLiteDatabase bdd;
    private QuizzSQLite quizzSQLite;

    public DAOQuestion(Context context) {
        quizzSQLite = QuizzSQLite.getInstance(context);
    }

    public void openForWrite() {
        bdd = quizzSQLite.getWritableDatabase();
    }

    private void openForRead() {
        bdd = quizzSQLite.getReadableDatabase();
    }

    public void close() {
        bdd.close();
    }

    public ArrayList<Question> getAllQuestionsByIdTheme(int idTheme, int difficulte) {

        openForRead();
        ArrayList<Question> listeQuestions = new ArrayList<>();
        // On prend également les questions avec une difficulté null ou légèrement inférieure
        // à celle sélectionnée uniquement car la base de données n'est actuellement pas assez fournie
        Cursor c = bdd.query(TABLE, new String[]{COL_ID, COL_LIBELLE, COL_IDTHEME},
                COL_IDTHEME + " = " + idTheme + " AND (" + COL_DIFFICULTE + " = " + difficulte
                + " OR " + COL_DIFFICULTE + " = " + (difficulte - 1) + " OR " + COL_DIFFICULTE + " IS NULL)",
                null, null, null, COL_LIBELLE);

        if (c.getCount() == 0) {
            c.close();
            return null;
        }

        while (c.moveToNext()) {
            Question question = cursorToQuestion(c);
            listeQuestions.add(question);
        }

        c.close();
        return listeQuestions;
    }

    private Question cursorToQuestion(Cursor c) {

        return new Question(c.getInt(0), c.getString(1));
    }

}
