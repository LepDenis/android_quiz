package fr.dl.quizzdenis.IHM;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.widget.EditText;
import android.widget.TextView;

import fr.dl.quizzdenis.R;


/*

---------- CLASSE NON UTILISEE ----------

*/


public class ScoreDialogFragment extends DialogFragment {

    private EditText etJoueur;
    private TextView tvWarning;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        //LayoutInflater inflater = getActivity().getLayoutInflater();
        //builder.setView(inflater.inflate(R.layout.dialog_score, null))
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {

            }
        })
                .setNegativeButton("Annuler", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        ScoreDialogFragment.this.getDialog().cancel();
                    }
                });
        AlertDialog dialog = builder.create();
        dialog.setContentView(R.layout.dialog_score);
        //etJoueur = findViewById(R.id.username);

        return dialog;
    }


}
