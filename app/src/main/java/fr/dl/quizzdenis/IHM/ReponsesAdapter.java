package fr.dl.quizzdenis.IHM;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.RadioButton;
import android.widget.TextView;

import java.util.ArrayList;

import fr.dl.quizzdenis.Metier.Reponse;
import fr.dl.quizzdenis.R;


/*

---------- CLASSE NON UTILISEE ----------

*/


public class ReponsesAdapter extends ArrayAdapter<Reponse> {

    private ArrayList<Reponse> reponses;
    private Context context;
    private int reponseAdapterID;

    public ReponsesAdapter(Context context, int reponseAdapterID
            , ArrayList<Reponse> reponses) {
        super(context, reponseAdapterID, reponses);
        this.reponses = reponses;
        this.context = context;
        this.reponseAdapterID = reponseAdapterID;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View view = convertView;

        if (view == null) {
            LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = layoutInflater.inflate(reponseAdapterID, parent, false);
        }
        final Reponse reponse = reponses.get(position);

        if (reponse != null) {
            final RadioButton rbReponse = view.findViewById(R.id.radioButton);
            final GridView gd = (GridView) rbReponse.getParent().getParent().getParent();

            System.out.println(gd);
            /*
            rbReponse.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View view) {

                    int i = 0;
                    while (i < gd.getCount()) {
                        RadioButton button = (RadioButton) gd.getItemAtPosition(i);
                        button.setChecked(false);
                    }
                    rbReponse.setChecked(true);
                }
            });
            */
            rbReponse.setText(reponse.getLibelle());
        }
        return view;
    }


}
