package fr.dl.quizzdenis.IHM;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;

import java.util.ArrayList;

import fr.dl.quizzdenis.DAO.DAOTheme;
import fr.dl.quizzdenis.Metier.Theme;
import fr.dl.quizzdenis.R;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        DAOTheme daoTheme = new DAOTheme(this);
        daoTheme.openForRead();
        ArrayList<Theme> themes = daoTheme.getAllThemes();
        GridView lvThemes = findViewById(R.id.listView);
        ThemesAdapter adapter = new ThemesAdapter(this, R.layout.themesadapter, themes);
        // ArrayAdapter adapter = new ArrayAdapter(this,android.R.layout.simple_list_item_1 ,themes);

        lvThemes.setAdapter(adapter);
        lvThemes.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                // récupération du thème sélectionné
                Theme selectedTheme = (Theme) parent.getItemAtPosition(position);

                Intent mainIntent = new Intent(MainActivity.this, QuestionnaireActivity.class);
                mainIntent.putExtra("theme", selectedTheme);
                startActivity(mainIntent);
            }
        });
        daoTheme.close();
    }

}
