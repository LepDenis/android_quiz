package fr.dl.quizzdenis.IHM;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import fr.dl.quizzdenis.DAO.DAOQuestion;
import fr.dl.quizzdenis.DAO.DAOReponse;
import fr.dl.quizzdenis.Metier.Question;
import fr.dl.quizzdenis.Metier.Reponse;
import fr.dl.quizzdenis.Metier.Theme;
import fr.dl.quizzdenis.R;

public class Questionnaire extends AppCompatActivity {

    private TextView tvQuestion;
    private TextView tvTheme;
    private TextView tvCommentaire;
    private TextView tvNumeroQuestion;
    private TextView tvScore;
    private RadioGroup rGroupe;
    private RadioButton rbA;
    private RadioButton rbB;
    private RadioButton rbC;
    private RadioButton rbD;
    private Button valider;

    private Theme themeActif;
    private ArrayList<Question> listeQuestions;
    private ArrayList<Reponse> listeReponses;
    private Question question;
    private int n;
    private int score;
    private boolean isOver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setContentView(R.layout.question);
        Intent themeIntent = getIntent();
        themeActif = themeIntent.getParcelableExtra("theme");

        tvQuestion = findViewById(R.id.question);
        tvTheme = findViewById(R.id.nomTheme);
        tvCommentaire = findViewById(R.id.commentaire);
        tvNumeroQuestion = findViewById(R.id.numeroQuestion);
        tvScore = findViewById(R.id.score);
        rGroupe = findViewById(R.id.reponses);
        rbA = findViewById(R.id.reponseA);
        rbB = findViewById(R.id.reponseB);
        rbC = findViewById(R.id.reponseC);
        rbD = findViewById(R.id.reponseD);
        valider = findViewById(R.id.valider);

        valider.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (rGroupe.getCheckedRadioButtonId() == -1) {
                    String noSelection = "Choisissez une réponse !!!";
                    tvCommentaire.setText(noSelection);

                } else {
                    if (rbA.isChecked() && listeReponses.get(0).getCorrecte() == 1)
                        score += 1;
                    if (rbB.isChecked() && listeReponses.get(1).getCorrecte() == 1)
                        score += 1;
                    if (rbC.isChecked() && listeReponses.get(2).getCorrecte() == 1)
                        score += 1;
                    if (rbD.isChecked() && listeReponses.get(3).getCorrecte() == 1)
                        score += 1;

                    tvCommentaire.setText("");
                    rGroupe.clearCheck();
                    genererQuestion();
                }
            }
        });

        if (savedInstanceState == null) {

            DAOQuestion daoQuestion = new DAOQuestion(this);
            listeQuestions = daoQuestion.getAllQuestionsByIdTheme(themeActif.getId(),0);

            n = 1;
            score = 0;
            isOver = false;

            tvTheme.setText(themeActif.getLibelle());
            genererQuestion();
        }
    }

    public void genererQuestion() {

        DAOReponse daoReponse = new DAOReponse(this);
        String affichageScore = "Score : " + score;
        tvScore.setText(affichageScore);
        String numero = "Question n°" + n + " :";
        Random random = new Random();

        if (listeQuestions.size() >= 1 && n <= 10) {

            tvNumeroQuestion.setText(numero);
            int index = random.nextInt(listeQuestions.size());
            question = listeQuestions.get(index);
            tvQuestion.setText(question.getLibelle());
            listeReponses = daoReponse.getAllReponsesByIdQuestion(question.getId());

            rbA.setText(listeReponses.get(0).getLibelle());
            rbB.setText(listeReponses.get(1).getLibelle());
            rbC.setText(listeReponses.get(2).getLibelle());
            rbD.setText(listeReponses.get(3).getLibelle());

            listeQuestions.remove(question);
            n++;

        } else {

            isOver = true;
            String finDeQuiz = "Quiz terminé ! \n\nVotre score est de " + score + " points.";
            tvQuestion.setText(finDeQuiz);
            tvNumeroQuestion.setVisibility(TextView.INVISIBLE);
            tvScore.setVisibility(TextView.INVISIBLE);
            tvCommentaire.setVisibility(TextView.INVISIBLE);
            rGroupe.setVisibility(View.INVISIBLE);
            valider.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
        savedInstanceState.putParcelable("themeactif", themeActif);
        savedInstanceState.putParcelableArrayList("listequestions", listeQuestions);
        savedInstanceState.putParcelable("question", question);
        savedInstanceState.putParcelableArrayList("listereponses", listeReponses);
        savedInstanceState.putInt("n", n);
        savedInstanceState.putInt("score", score);
        savedInstanceState.putBoolean("isover", isOver);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        isOver = savedInstanceState.getBoolean("isover");
        themeActif = savedInstanceState.getParcelable("themeactif");
        tvTheme.setText(themeActif.getLibelle());
        score = savedInstanceState.getInt("score");

        if (isOver) {
            String finDeQuiz = "Quiz terminé ! \n\nVotre score est de " + score + " points.";
            tvQuestion.setText(finDeQuiz);
            tvNumeroQuestion.setVisibility(TextView.INVISIBLE);
            tvScore.setVisibility(TextView.INVISIBLE);
            tvCommentaire.setVisibility(TextView.INVISIBLE);
            rGroupe.setVisibility(View.INVISIBLE);
            valider.setVisibility(View.INVISIBLE);

        } else {

            listeQuestions = savedInstanceState.getParcelableArrayList("listequestions");
            question = savedInstanceState.getParcelable("question");
            listeReponses = savedInstanceState.getParcelableArrayList("listereponses");
            n = savedInstanceState.getInt("n");
            String numero = "Question n°" + n + " :";
            tvNumeroQuestion.setText(numero);
            tvQuestion.setText(question.getLibelle());
            rbA.setText(listeReponses.get(0).getLibelle());
            rbB.setText(listeReponses.get(1).getLibelle());
            rbC.setText(listeReponses.get(2).getLibelle());
            rbD.setText(listeReponses.get(3).getLibelle());
            String affichageScore = "Score : " + score;
            tvScore.setText(affichageScore);
        }
    }

}
