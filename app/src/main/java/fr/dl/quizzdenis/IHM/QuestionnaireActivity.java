package fr.dl.quizzdenis.IHM;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.text.InputFilter;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Space;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import fr.dl.quizzdenis.Metier.HighScore;
import fr.dl.quizzdenis.Metier.Partie;
import fr.dl.quizzdenis.Metier.Question;
import fr.dl.quizzdenis.Metier.Reponse;
import fr.dl.quizzdenis.Metier.Theme;
import fr.dl.quizzdenis.R;

public class QuestionnaireActivity extends AppCompatActivity {

    private TextView tvQuestion;
    private TextView tvTheme;
    private TextView tvCommentaire;
    private TextView tvNumeroQuestion;
    private TextView tvScore;
    private RadioGroup rGroupe;
    private Button valider;
    private Partie partie;
    private boolean isReponseConfirmed;
    private boolean isListeQuestions;
    private boolean isDifficulteSet;
    private boolean isHighScoreSaisi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initialiser();

        // Cas où l'on ne restaure pas un état précédemment sauvegardé de l'activité
        if (savedInstanceState == null) {
            isDifficulteSet = false;
            choisirDifficulte();
            tvTheme.setText(partie.getThemeActif().getLibelle());
        }
        // Permet une temporisation lors de la validation de la réponse afin de montrer qu'elle était la bonne réponse
        final Runnable runnable = new Runnable() {
            @Override
            public void run() {
                valider.setVisibility(View.VISIBLE);
                tvCommentaire.setText("");
                isReponseConfirmed = false;
                if (!partie.isOver())
                    afficherQuestion();
                else
                    terminerPartie();
            }
        };
        final Handler handler = new Handler();

        // Ajout d'un écouteur sur le bouton de confirmation
        valider.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int checkedId = rGroupe.getCheckedRadioButtonId();
                // On vérifie qu'une réponse a bien été sélectionnée
                if (checkedId == -1) {
                    String noSelection = "Choisissez une réponse !!!";
                    tvCommentaire.setText(noSelection);
                } else {
                    RadioButton rbChecked = (RadioButton) rGroupe.getChildAt(checkedId);
                    isReponseConfirmed = true;
                    // On vérifie si la réponse choisie est la bonne, on montre la bonne réponse puis on affiche la question suivante
                    montrerBonneReponse(rbChecked);
                    partie.genererQuestion();
                    handler.postDelayed(runnable, 1800);
                }
            }
        });
    }

    // Initialise l'activité et récupère le thème choisi
    public void initialiser() {

        setContentView(R.layout.questionnaire);
        Intent themeIntent = getIntent();
        partie = new Partie(this);
        Theme themeActif = themeIntent.getParcelableExtra("theme");
        partie.setThemeActif(themeActif);
        tvQuestion = findViewById(R.id.question);
        tvTheme = findViewById(R.id.nomTheme);
        tvCommentaire = findViewById(R.id.commentaire);
        tvNumeroQuestion = findViewById(R.id.numeroQuestion);
        tvScore = findViewById(R.id.score);
        rGroupe = findViewById(R.id.groupReponses);
        valider = findViewById(R.id.valider);
    }

    // Génère les boutons de choix de la difficulté
    public RadioButton afficherDifficulte(int id, String text) {

        RadioButton rb = new RadioButton(this);
        rb.setText(text);
        rb.setTextSize(18);
        rb.setPaddingRelative(16, 16, 16, 16);
        rb.setId(id);

        return rb;
    }

    // Permet de choisir la difficulté du quizz
    public void choisirDifficulte() {

        String choix = "Veuillez choisir un niveau de difficulté (les quantités peuvent varier selon les enregistrements actuellement dans la base de données) :";
        tvNumeroQuestion.setText(choix);
        tvQuestion.setVisibility(View.INVISIBLE);
        valider.setVisibility(View.INVISIBLE);
        // On affiche un layout du choix de la difficulté (3 radio buttons de niveau + bouton de validation)
        RadioButton facile = afficherDifficulte(1, "Facile \n(5 questions / 3 propositions)");
        RadioButton moyen = afficherDifficulte(2, "Moyen \n(10 questions / 4 propositions)");
        RadioButton difficile = afficherDifficulte(3, "Difficile \n(15 questions / 5 propositions)");
        Space space = new Space(this);
        space.setMinimumHeight(16);

        Button ok = new Button(this);
        String confirm = "OK";
        ok.setText(confirm);
        ok.setTextSize(16);
        ok.setPaddingRelative(16, 16, 16, 16);

        rGroupe.addView(facile);
        rGroupe.addView(moyen);
        rGroupe.addView(difficile);
        rGroupe.addView(space);
        rGroupe.addView(ok);
        // Ajout d'un écouteur sur le bouton OK
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int choixId = rGroupe.getCheckedRadioButtonId();
                if (choixId == -1) {
                    String choisir = "Veuillez sélectionner un niveau de difficulté !";
                    tvCommentaire.setText(choisir);
                } else {
                    // On paramètre la difficulté (nombre de questions total + nombre de propositions de réponse) qui est équivalente à l'id d'un RadioButton
                    partie.setDifficulte(choixId);
                    partie.setNbrQuestions(choixId);
                    isDifficulteSet = true;
                    tvCommentaire.setText("");
                    afficherNiveau();
                    demarrerQuiz();
                }
            }
        });
    }

    // Gère l'affichage du jeu après avoir choisi un niveau de difficulté
    public void demarrerQuiz() {

        partie.initialiser();
        // On vérifie que la liste de question liée au thème sélectionné n'est pas null
        if (partie.getThemeActif().getQuestions() != null && partie.getDifficulte() != 0) {
            isListeQuestions = true;
            tvQuestion.setVisibility(View.VISIBLE);
            valider.setVisibility(View.VISIBLE);
            partie.genererQuestion();
            afficherQuestion();
        } else {
            // Affichage d'un message si c'est le cas
            tvQuestion.setVisibility(View.VISIBLE);
            rGroupe.setVisibility(View.INVISIBLE);
            tvNumeroQuestion.setVisibility(View.INVISIBLE);
            afficherNoQuestion();
        }
    }

    // Affiche la question active générée par l'instance de la classe Partie
    public void afficherQuestion() {

        String affichageScore = "Score : " + partie.getScore();
        tvScore.setText(affichageScore);

        String numero = "Question n°" + partie.getNumQuestion() + " :";
        tvNumeroQuestion.setText(numero);
        tvQuestion.setText(partie.getQuestionActive().getLibelle());
        afficherReponses();

    }

    // Affiche les réponses de la question active
    public void afficherReponses() {

        // Réinitialisation du RadioGroup
        rGroupe.clearCheck();
        if (rGroupe.getChildCount() != 0)
            rGroupe.removeAllViews();
        // Affichage des réponses
        for (int i = 0; i < partie.getQuestionActive().getReponses().size(); i++) {
            RadioButton rb = new RadioButton(this);
            rb.setTextSize(18);
            rb.setPaddingRelative(8, 8, 8, 8);
            rGroupe.addView(rb);
            rb.setId(i);
            rb.setText(partie.getQuestionActive().getReponses().get(i).getLibelle());
            rb.setClickable(true);
        }
    }

    // Renvoie à l'utilisateur s'il a choisi la bonne réponse
    public void montrerBonneReponse(View view) {

        String right = "Bonne réponse !";
        String wrong = "Mauvaise réponse !";
        valider.setVisibility(View.INVISIBLE);
        // On vérifie si la réponse est juste, le score sera incrémenté le cas échéant
        if (partie.verifierReponse(view))
            tvCommentaire.setText(right);
        else
            tvCommentaire.setText(wrong);

        setRadioColor();
    }

    // Gère la couleur du texte des Radio Buttons lors du focus sur la bonne réponse
    public void setRadioColor() {

        for (int m = 0; m < rGroupe.getChildCount(); m++) {
            RadioButton button = (RadioButton) rGroupe.getChildAt(m);
            // Si la réponse est fausse, elle devient rouge
            if (partie.getQuestionActive().getReponses().get(m).getCorrecte() == 0)
                button.setTextColor(Color.RED);
            else
                // sinon elle devient verte
                button.setTextColor(Color.parseColor("#228B22"));

            button.setClickable(false);
        }
    }

    // Affiche la vue de fin de partie et le score obtenu
    public void terminerPartie() {

        String finDeQuiz = "Quiz terminé ! \n\nVotre score est de " + partie.getScore() + " bonnes réponses sur " + partie.getNumQuestion() + " questions !\n\n";
        tvQuestion.setText(finDeQuiz);
        tvNumeroQuestion.setVisibility(TextView.INVISIBLE);
        tvScore.setVisibility(TextView.INVISIBLE);
        tvCommentaire.setVisibility(TextView.INVISIBLE);
        valider.setVisibility(View.INVISIBLE);
        afficherHighScores();
    }

    // Génère les TextView pour afficher les scores
    public TextView genererVueHighScore(int id, String texte) {
        TextView tv = new TextView(this);
        tv.setText(texte);
        tv.setId(id);
        tv.setTextSize(16);
        tv.setPaddingRelative(16, 16, 16, 16);
        return tv;
    }

    // Affiche la liste des meilleurs scores
    public void afficherHighScores() {

        rGroupe.removeAllViews();
        String highScores = "Podium : ";
        TextView tvHigh = genererVueHighScore(0, highScores);
        rGroupe.addView(tvHigh);

        ArrayList<HighScore> listeScores = partie.getHighScores(partie.getThemeActif().getId(), partie.getDifficulte());
        // On affiche les meilleurs scores liés au thème et à la difficulté choisis
        if (listeScores != null) {
            afficherPodium(listeScores);
            // Cas où le joueur vient de faire un score à afficher sur les prochains podiums
            if (((partie.getScore() > listeScores.get(listeScores.size() - 1).getScore()) || (listeScores.size() < 3)) && !isHighScoreSaisi)
                afficherSaisieNom();
        } else {
            // ou un message s'il n'y en a pas encore
            String chaineNoScore = "Aucun meilleur score trouvé.";
            TextView tvAucun = genererVueHighScore(1, chaineNoScore);
            rGroupe.addView(tvAucun);
            if (!isHighScoreSaisi)
                afficherSaisieNom();
        }
    }

    // Affiche le podium
    public void afficherPodium(ArrayList<HighScore> listeScores) {

        for (int i = 0; i < listeScores.size(); i++) {
            HighScore score = listeScores.get(i);
            String chaineScore = (i + 1) + " - " + score.getNomJoueur() + " - " + score.getScore() + " points";
            TextView tvScore = genererVueHighScore((i + 1), chaineScore);
            rGroupe.addView(tvScore);
        }
    }

    // Affiche un message dans le cas où aucune question n'est liée au thème sélectionné
    public void afficherNoQuestion() {

        valider.setVisibility(View.INVISIBLE);
        String aucuneQuestion = "Aucune question correspondant au thème \"" + partie.getThemeActif().getLibelle() + "\" n\'a été trouvée dans la base de données.";
        tvQuestion.setText(aucuneQuestion);
        tvCommentaire.setText("");
        isListeQuestions = false;
    }

    // Affiche la difficulté choisie à côté du thème choisi, dans la partie supérieure de la vue
    public void afficherNiveau() {

        if (partie.getDifficulte() == 1) {
            String facile = tvTheme.getText() + " - Facile";
            tvTheme.setText(facile);
        }
        if (partie.getDifficulte() == 2) {
            String moyen = tvTheme.getText() + " - Moyen";
            tvTheme.setText(moyen);
        }
        if (partie.getDifficulte() == 3) {
            String difficile = tvTheme.getText() + " - Difficile";
            tvTheme.setText(difficile);
        }
    }

    // Affiche l'EditText et le bouton de confirmation en cas de meilleur score, et le retourne
    public void afficherSaisieNom() {

        final EditText etNomJoueur = genererEditText();
        rGroupe.addView(etNomJoueur);

        final Button ok = genererButtonOK();
        rGroupe.addView(ok);

        // On renvoie un HighScore à l'instance de la classe partie, seul son champ nomJoueur sera initialisé
        final HighScore highScore = new HighScore();
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (etNomJoueur.getText().toString().trim().length() > 0) {
                    highScore.setNomJoueur(etNomJoueur.getText().toString().trim());
                    etNomJoueur.setVisibility(View.INVISIBLE);
                    ok.setVisibility(View.INVISIBLE);
                    partie.addScore(highScore);
                    isHighScoreSaisi = true;
                    rGroupe.removeAllViews();

                    ArrayList<HighScore> listeScores = partie.getHighScores(partie.getThemeActif().getId(), partie.getDifficulte());
                    // On réaffiche les meilleurs scores liés au thème et à la difficulté choisie
                    if (listeScores != null)
                        afficherPodium(listeScores);
                } else {
                    Toast myToast = Toast.makeText(getApplicationContext(), "Veuillez entrer un nom valide !", Toast.LENGTH_LONG);
                    myToast.show();
                }
            }
        });
    }

    // Génération de l'EditText permettant de récupérer le nom du joueur ayant fait un HighScore
    private EditText genererEditText() {

        EditText et = new EditText(this);
        et.setHint("Nouveau Meilleur Score ! Entrer votre nom...");
        et.setTextSize(18);
        et.setMinHeight(24);
        et.setMinWidth(128);
        et.setSingleLine();
        InputFilter[] filters = new InputFilter[1];
        // On limite le nombre de caractères pour le nom du joueur à 10
        filters[0] = new InputFilter.LengthFilter(10);
        et.setFilters(filters);

        return et;
    }

    // Génération du bouton OK permettant de confirmr le HighScore
    private Button genererButtonOK() {

        Button ok = new Button(this);
        String confirm = "OK";
        ok.setText(confirm);
        ok.setTextSize(18);
        ok.setPaddingRelative(16, 16, 16, 16);

        return ok;
    }

    // ---------- Méthodes remplaçables par quelques lignes dans le manifest (mais ça fait écrire du java !) ---------- \\
    // Sauvegarde de l'état de l'activité
    @Override
    protected void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
        savedInstanceState.putParcelable("themeactif", partie.getThemeActif());
        savedInstanceState.putBoolean("isdifficulteset", isDifficulteSet);
        savedInstanceState.putInt("difficulte", partie.getDifficulte());
        savedInstanceState.putInt("n", partie.getNumQuestion());
        savedInstanceState.putBoolean("ishighscore", isHighScoreSaisi);

        if (isListeQuestions) {
            savedInstanceState.putParcelableArrayList("listequestions", partie.getThemeActif().getQuestions());
            if (partie.getQuestionActive() != null) {
                savedInstanceState.putParcelable("question", partie.getQuestionActive());
                savedInstanceState.putParcelableArrayList("listereponses", partie.getQuestionActive().getReponses());
            }
            savedInstanceState.putInt("score", partie.getScore());
            savedInstanceState.putBoolean("isover", partie.isOver());
            savedInstanceState.putBoolean("isreponseconfirmed", isReponseConfirmed);
            savedInstanceState.putBoolean("islistequestions", isListeQuestions);

            String commentaires = tvCommentaire.getText().toString();
            savedInstanceState.putString("tvcommentaires", commentaires);
            int indexChecked = rGroupe.getCheckedRadioButtonId();
            savedInstanceState.putInt("checkedbutton", indexChecked);
        }
    }

    // Restauration de l'état sauvegardé
    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        isListeQuestions = savedInstanceState.getBoolean("islistequestions");
        isDifficulteSet = savedInstanceState.getBoolean("isdifficulteset");
        Theme sauvegardeTheme = savedInstanceState.getParcelable("themeactif");
        partie.setThemeActif(sauvegardeTheme);
        tvTheme.setText(partie.getThemeActif().getLibelle());
        partie.setNumQuestion(savedInstanceState.getInt("n"));
        isHighScoreSaisi = savedInstanceState.getBoolean("ishighscore");

        if (isListeQuestions) {
            partie.setOver(savedInstanceState.getBoolean("isover"));
            partie.setScore(savedInstanceState.getInt("score"));
            partie.setDifficulte(savedInstanceState.getInt("difficulte"));

            if (partie.isOver()) {
                afficherNiveau();
                terminerPartie();
            } else {
                ArrayList<Question> sauvegardeListeQuestions = savedInstanceState.getParcelableArrayList("listequestions");
                partie.getThemeActif().setQuestions(sauvegardeListeQuestions);
                Question sauvegardeQuestionActive = savedInstanceState.getParcelable("question");
                partie.setQuestionActive(sauvegardeQuestionActive);
                ArrayList<Reponse> sauvegardeListeReponses = savedInstanceState.getParcelableArrayList("listereponses");
                partie.getQuestionActive().setReponses(sauvegardeListeReponses);
                partie.setNbrQuestions(savedInstanceState.getInt("difficulte"));
                afficherNiveau();
                afficherQuestion();

                int indexChoisi = savedInstanceState.getInt("checkedbutton");
                isReponseConfirmed = savedInstanceState.getBoolean("isreponseconfirmed");
                RadioButton rbChoisi = (RadioButton) rGroupe.getChildAt(indexChoisi);
                if (rbChoisi != null && !isReponseConfirmed && indexChoisi != (-1))
                    rbChoisi.setChecked(true);
                else {
                    rGroupe.clearCheck();
                    isReponseConfirmed = false;
                }
            }
        } else {
            if (isDifficulteSet)
                afficherNoQuestion();
            else
                choisirDifficulte();
        }
    }

}
