package fr.dl.quizzdenis.Metier;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

public class Theme implements Parcelable {

    private int id;
    private String libelle;
    private String icone;
    private ArrayList<Question> questions;

    // Constructeurs
    public Theme() {

    }

    protected Theme(Parcel in) {
        id = in.readInt();
        libelle = in.readString();
        icone = in.readString();
        questions = in.readArrayList(Question.class.getClassLoader());
    }

    public Theme(int id, String libelle, String icone) {
        this.id = id;
        this.libelle = libelle;
        this.icone = icone;
    }

    // Accesseurs
    public ArrayList<Question> getQuestions() {
        return questions;
    }

    public void setQuestions(ArrayList<Question> questions) {
        this.questions = questions;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLibelle() {
        return libelle;
    }

    public String getIcone() {
        return icone;
    }

    // Méthodes de l'interface Parcelable
    public static final Creator<Theme> CREATOR = new Creator<Theme>() {
        @Override
        public Theme createFromParcel(Parcel in) {
            return new Theme(in);
        }

        @Override
        public Theme[] newArray(int size) {
            return new Theme[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(libelle);
        dest.writeString(icone);
        dest.writeList(questions);
    }

    @Override
    public String toString() {
        return libelle;
    }

}
