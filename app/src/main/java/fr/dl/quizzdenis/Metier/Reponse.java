package fr.dl.quizzdenis.Metier;

import android.os.Parcel;
import android.os.Parcelable;

public class Reponse implements Parcelable {

    private int id;
    private String libelle;
    private int correcte;

    // Constructeurs
    public Reponse() {

    }

    public Reponse(int id, String libelle, int correcte) {
        this.id = id;
        this.libelle = libelle;
        this.correcte = correcte;
    }

    protected Reponse(Parcel in) {
        id = in.readInt();
        libelle = in.readString();
        correcte = in.readInt();
    }

    // Accesseurs
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLibelle() {
        return libelle;
    }

    public int getCorrecte() {
        return correcte;
    }

    // Méthodes de l'interface Parcelable
    public static final Creator<Reponse> CREATOR = new Creator<Reponse>() {
        @Override
        public Reponse createFromParcel(Parcel in) {
            return new Reponse(in);
        }

        @Override
        public Reponse[] newArray(int size) {
            return new Reponse[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(libelle);
        dest.writeInt(correcte);
    }

    @Override
    public String toString() {
        return libelle;
    }

}
