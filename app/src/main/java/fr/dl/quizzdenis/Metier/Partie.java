package fr.dl.quizzdenis.Metier;

import android.content.Context;
import android.view.View;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Random;

import fr.dl.quizzdenis.DAO.DAOHighScore;
import fr.dl.quizzdenis.DAO.DAOQuestion;
import fr.dl.quizzdenis.DAO.DAOReponse;

public class Partie {

    private Theme themeActif;
    private Question questionActive;
    private int numQuestion;
    private int nbrQuestions;
    private int difficulte;
    private int score;
    private boolean isOver;
    private Context myContext;

    // Constructeur
    public Partie(Context context) {
        this.myContext = context;
    }

    // Initialise la partie et récupère les questions liées au thème choisi
    public void initialiser() {

        DAOQuestion daoQuestion = new DAOQuestion(myContext);
        ArrayList<Question> questions = daoQuestion.getAllQuestionsByIdTheme(themeActif.getId(), difficulte);
        if (questions != null) {
            themeActif.setQuestions(questions);
            daoQuestion.close();
        }
        numQuestion = 0;
        score = 0;
        isOver = false;
    }

    // Choisit une question aléatoirement dans la liste puis la supprime
    public void genererQuestion() {

        DAOReponse daoReponse = new DAOReponse(myContext);
        Random random = new Random();

        if (themeActif.getQuestions() != null) {
            if (themeActif.getQuestions().size() > 0 && numQuestion < nbrQuestions) {

                int index = random.nextInt(themeActif.getQuestions().size());
                questionActive = themeActif.getQuestions().get(index);
                themeActif.getQuestions().remove(questionActive);
                // Récupération des réponses et classement de manière aléatoire
                ArrayList<Reponse> reponsesTemp = daoReponse.getAllReponsesByIdQuestion(questionActive.getId());
                ArrayList<Reponse> reponses = organiserReponses(reponsesTemp);
                questionActive.setReponses(reponses);
                numQuestion++;
                daoReponse.close();
            } else
                isOver = true;
        }
    }

    // Organise un nombre de réponses aléatoirement selon la difficulté
    private ArrayList<Reponse> organiserReponses(ArrayList<Reponse> liste) {

        // Instanciation d'une 3e liste temporaire, sert uniquement pour le classement des réponses
        ArrayList<Reponse> answerList = new ArrayList<>();
        int nbrReponse;
        // Pas encore assez d'enregistrements pour ce thème
        if (themeActif.getLibelle().equals("South Park") && difficulte == 3)
            nbrReponse = 4;
        else
            nbrReponse = 2 + difficulte;

        Random random = new Random();
        int i = 0;
        boolean isReponseTrouvee = false;
        // On ajoute la bonne réponse à la liste temporaire
        while (!isReponseTrouvee && i < liste.size()) {
            if (liste.get(i).getCorrecte() == 1) {
                answerList.add(liste.get(i));
                liste.remove(liste.get(i));
                isReponseTrouvee = true;
            }
            i++;
        }
        // On y ajoute un certain nombre de mauvaises réponses (selon la difficulté)
        for (int j = 1; j < nbrReponse; j++) {
            int position = random.nextInt(liste.size());
            answerList.add(liste.get(position));
            liste.remove(liste.get(position));
        }
        // On remplit la liste finale d'enregistrement null afin d'avoir un nombre d'index égal à celui de la liste temporaire
        ArrayList<Reponse> listeTriee = new ArrayList<>();
        for (int l = 0; l < nbrReponse; l++) {
            listeTriee.add(l, new Reponse());
        }
        // On donne un index aléatoire à chaque réponse qu'on ajoute à la liste finale
        for (int k = 0; k < answerList.size(); k++) {
            int nouvellePosition = random.nextInt(answerList.size());
            // On change de chiffre si la réponse à l'index généré n'est pas null
            while (listeTriee.get(nouvellePosition).getLibelle() != null) {
                nouvellePosition = random.nextInt(answerList.size());
            }
            listeTriee.remove(nouvellePosition);
            listeTriee.add(nouvellePosition, answerList.get(k));
        }

        return listeTriee;
    }

    // Vérifie si l'on a choisi la bonne réponse et incrémente le score le cas échéant
    public boolean verifierReponse(View view) {

        boolean isReponseVerifiee = false;
        // L'id du bouton sélectionné équivaut à l'index de la réponse correspondante dans la liste de propositions
        if (questionActive.getReponses().get(view.getId()).getCorrecte() == 1) {
            score += 1;
            isReponseVerifiee = true;
        }
        return isReponseVerifiee;
    }

    // Permet de récupérer les meilleurs scores
    public ArrayList<HighScore> getHighScores(int idTheme, int difficulte) {

        ArrayList<HighScore> listeScores;
        DAOHighScore daoHighScore = new DAOHighScore(myContext);
        listeScores = daoHighScore.getAllHighScores(idTheme, difficulte);
        daoHighScore.close();
        return listeScores;
    }

    // Permet d'ajouter un meilleur score (méthode non écrite)
    public void addScore(HighScore highScore) {

        highScore.setIdTheme(themeActif.getId());
        highScore.setDifficulte(difficulte);
        highScore.setScore(score);

        DAOHighScore daoHighScore = new DAOHighScore(myContext);
        if (!daoHighScore.insertNewScore(highScore)) {
            Toast myToast = Toast.makeText(myContext, "Erreur lors de l'enregistrement du nouveau score", Toast.LENGTH_LONG);
            myToast.show();
        }
        daoHighScore.close();
    }

    // Accesseurs
    public Theme getThemeActif() {
        return themeActif;
    }

    public void setThemeActif(Theme themeActif) {
        this.themeActif = themeActif;
    }

    public int getNumQuestion() {
        return numQuestion;
    }

    public void setNumQuestion(int numQuestion) {
        this.numQuestion = numQuestion;
    }

    public void setNbrQuestions(int nombre) {
        switch (nombre) {
            case 1:
                nbrQuestions = 5;
                break;
            case 2:
                nbrQuestions = 10;
                break;
            case 3:
                nbrQuestions = 15;
                break;
        }
    }

    public int getDifficulte() {
        return difficulte;
    }

    public void setDifficulte(int difficulte) {
        this.difficulte = difficulte;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public boolean isOver() {
        return isOver;
    }

    public void setOver(boolean over) {
        isOver = over;
    }

    public Question getQuestionActive() {
        return questionActive;
    }

    public void setQuestionActive(Question questionActive) {
        this.questionActive = questionActive;
    }

}
