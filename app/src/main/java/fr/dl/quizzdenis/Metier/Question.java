package fr.dl.quizzdenis.Metier;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

public class Question implements Parcelable {

    private int id;
    private String libelle;
    private ArrayList<Reponse> reponses;

    // Constructeurs
    public Question(int id, String libelle) {
        this.id = id;
        this.libelle = libelle;
        this.reponses = new ArrayList<>();
    }

    protected Question(Parcel in) {
        id = in.readInt();
        libelle = in.readString();
        reponses = in.readArrayList(Reponse.class.getClassLoader());
    }

    // Accesseurs
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLibelle() {
        return libelle;
    }

    public ArrayList<Reponse> getReponses() {
        return reponses;
    }

    public void setReponses(ArrayList<Reponse> reponses) {
        this.reponses = reponses;
    }

    // Méthodes de l'interface Parcelable
    public static final Creator<Question> CREATOR = new Creator<Question>() {
        @Override
        public Question createFromParcel(Parcel in) {
            return new Question(in);
        }

        @Override
        public Question[] newArray(int size) {
            return new Question[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(libelle);
        dest.writeList(reponses);
    }

    @Override
    public String toString() {
        return libelle;
    }

}
