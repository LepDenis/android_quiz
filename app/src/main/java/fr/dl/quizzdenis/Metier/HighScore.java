package fr.dl.quizzdenis.Metier;

public class HighScore {

    private int id;
    private String nomJoueur;
    private int score;
    private int idTheme;
    private int difficulte;

    // Constructeurs
    public HighScore() {

    }

    public HighScore(int id, String nomJoueur, int score, int idTheme, int difficulte) {
        this.id = id;
        this.nomJoueur = nomJoueur;
        this.score = score;
        this.idTheme = idTheme;
        this.difficulte = difficulte;
    }

    // Accesseurs
    public String getNomJoueur() {
        return nomJoueur;
    }

    public void setNomJoueur(String nomJoueur) {
        this.nomJoueur = nomJoueur;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdTheme() {
        return idTheme;
    }

    public void setIdTheme(int idTheme) {
        this.idTheme = idTheme;
    }

    public int getDifficulte() {
        return difficulte;
    }

    public void setDifficulte(int difficulte) {
        this.difficulte = difficulte;
    }

    @Override
    public String toString() {
        return nomJoueur + " - " + idTheme + " - " + difficulte + " - " + score;
    }

}
